import React from "react";
import {Text, StyleSheet, ScrollView, FlatList,View} from "react-native";

import ListItem from "../ListItem/ListItem";

function renderItem(onItemDeleted, {item, index}) {
    return <ListItem
        placeName={item}
        onItemPressed={() => onItemDeleted(index)}
    />
}

function keyExtractor(item, index) {
    return index.toString();
}

const placeList = props => {
    //const placesOutput = props.places.map((place, i) => (
    //  <ListItem
    //    key={i}
    //    placeName={place}
    //    onItemPressed={() => props.onItemDeleted(i)}
    //  />
    //));
    return <FlatList
        style={styles.listContainer}
        data={props.places}
        keyExtractor={keyExtractor}
        renderItem={renderItem.bind(null,props.onItemDeleted)}
    />
    //alert(typeof props.places[0]);
    //return <View></View>

    //return <ScrollView style={styles.listContainer}>{placesOutput}</ScrollView>;
};

const styles = StyleSheet.create({
    listContainer: {
        width: "100%"
    }
});

export default placeList;
